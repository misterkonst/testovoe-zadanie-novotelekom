# **"Системные требования" для Laravel 5.4**
мин PHP >= 5.6.4 (PHP 7.1.* желательно)
https://laravel.com/docs/5.4#web-server-configuration

# **Установка и настройка**
- точка вхождения /public/index.php (для "VirtualHost")
- git clone
- [console] - composer install
- если отсутствует файл .env (если все "ОК" пропускаем), то скопировать .env.example > .env
- [console] - php artisan key:generate (Еще раз генерируем ключик)
- создаем БД с произвольным навзанием
- наистраивам по примеру ниже (см. файл .enf - настройка фреймворка и  подключения к базе)
- [console] - php artisan migrate (накатываем миграции)
- если все ок, то все готово к использованию.

###### файл .enf - настройка фреймворка и  подключения к базе
    * APP_DEBUG=false
    * DB_HOST=127.0.0.1 - локалхост IP
    * DB_PORT=3306 - стандартный порт подключения к базе
    * DB_DATABASE=databasename - название базы
    * DB_USERNAME=logindatabase - логин подключения к базе
    * DB_PASSWORD=password - пароль подключения к базе

#  **Краткое пояснение**
- Использовал стандартную стрктуру фреймворка
- /app/http/controllers -> контроллеры
- /app/http/controllers/forms/ -> обработка формы
- /app/Models/ -> модель
- /database/migrations -> миграция
- /resources/views -> все вьюхи (писал без blade)
- /routes/web.php -> файл роутера
- в структуре DB сложно запутаться
поле URl намерено имеет тип varchar, т.к. это тестовый проект и сделано это было специально для показа работы валидатора. В противном случае поле имело бы тип text.