<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::group(['middleware' => 'web'], function () {
    //main page
    Route::get('/', [
        'uses' => 'MainController@index',
    ]);
    //redirect if isset custom parameter
    Route::get('/{short_url}', [
        'uses' => 'MainController@redirectWebsite',
    ]);
    //form route
    Route::post('form-shortener', 'forms\FormShorter@ajaxFormAdd');
});
