<div class="container">
    <div class="row justify-content-center">
        <div class="col-8">
            <h1 class="text-center main_title"><?= $title ?></h1>
        </div>
    </div>

    <div class="row justify-content-center">
        <div class="col-md-4">
            <form id="submit_form" method="post" action="/form-shortener">
                <div class="form-group">
                    <label> Сокращение cсылок </label>
                    <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
                    <input type="text" name="long_url" class="form-control" placeholder="Ссылка, которую вы хотите сократить">
                    <small class="form-text text-muted">На этой странице Вы можете сделать из длинной и сложной ссылки простую.</small>
                </div>
                <?= csrf_field() ?>
                <button type="submit" class="btn btn-primary" id="button-text">Укоротить</button>
            </form>
        </div>
    </div>

    <!--noindex-->
    <div class="row justify-content-center short_block" style="display:none">
        <div class="col-md-4">
            <small class="text-muted">Ваша ссылка:</small>
            <div class="short_block_link"></div>
            <small class="text-muted copy_block" style="display:none">Cкопировано!</small>
        </div>
    </div>
    <!--/noindex-->

</div>
