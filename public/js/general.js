/* 
 Created on : 08.01.2018
 Author     : misterkonst@yandex.ru
 */
$(document).ready(function () {
//start document ready

    $('#submit_form').submit(function (t) {
        t.preventDefault();
        if (!$('#button-text').hasClass('disabled')) {

            $('.copy_block').hide();
            $('#button-text').addClass('disabled').html('Укорачивается');
            var form_send_to = $('#submit_form').attr('action');
            var _token = $('#submit_form').find("input[name='_token']").val();
            var url = $('#submit_form').find("input[name*='long_url']").val();

            //start ajax
            $.ajax({
                url: form_send_to,
                type: 'POST',
                data: {_token: _token, url: url},
                success: function (data) {
                        $('#submit_form').find("input[name*='long_url']").val('');
                        $('.print-error-msg').find('ul').html('');
                        $('.print-error-msg').css('display', 'none');
                        $('.short_block_link').html(data.domain + '/' + data.short);
                        $('.short_block').show();
                        $('#button-text').removeClass('disabled').html('Укоротить');
                },
                error: function(data) {
                    printErrorMsg(data);
                    $('#button-text').removeClass('disabled').html('Укоротить');
                }
            });
            //end ajax
        }
    });

    //Копирование в буфер обмена clipboard.js
    $('.short_block_link').click(function (t) {
        var clipboard = new Clipboard('.short_block_link', {
            target: function () {
                $('.copy_block').show();
                return document.querySelector('.short_block_link');
            }
        });
    });

    //вывод ошибок
    function printErrorMsg(data) {
        var msg = data.responseJSON;
        $('.print-error-msg').find('ul').html('');
        $('.print-error-msg').css('display', 'block');
        $.each(msg, function (key, value) {
            $('.print-error-msg').find('ul').append('<li>' + value + '</li>');
        });
    }

//end document ready
});