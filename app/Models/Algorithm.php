<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//Алгоритм создания уникальных ссылок
class Algorithm extends Model
{

    //кодирование на основе полученной id после сохранения ссылки
    static function encode($id)
    {
        //id полеченный при сохранении мы переводим в 36-тиричную систему исчесления
        return base_convert($id, 10, 36);
    }

    //декодирования для получения id ссылок
    static function decode($encode_id)
    {
        //36-ти ричное значение переводим обратно в 10-тиричное
        return base_convert($encode_id, 36, 10);
    }

}
