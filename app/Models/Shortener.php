<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shortener extends Model
{

    protected $fillable = ['url'];

    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'shortener';


}
