<?php

namespace App\Http\Controllers\forms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use \App\Models\Shortener as Shortener;
use \App\Models\Algorithm as Alg;
use \App\Http\Requests\formValidation;


class FormShorter extends Controller
{

    public function ajaxFormAdd(formValidation $request)
    {
        $resp = Shortener::create($request->all());
        //кодируем id полученное после сохранения data
        $enc = Alg::encode($resp->id);

        return response()->json([
            'success' => 'Added new records.',
            'domain' => \Request::root(),
            'short' => strtoupper($enc),
        ]);
    }

}
