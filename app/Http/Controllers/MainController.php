<?php

namespace App\Http\Controllers;

use App\Models\Shortener as Shortener;
use \App\Models\Algorithm as Alg;

class MainController extends Controller
{

    //render main page
    public function index()
    {
        $data = [
            'content' => 'index',
            'title' => 'Пецкалев К.В. тестовое задание',
        ];
        return view('layouts.index', $data);
    }

    //redirect if isset custom parameter
    public function redirectWebsite($short_url = null)
    {
        $resp = Shortener::whereId(Alg::decode($short_url))->first();
        if ($resp) {
            return redirect($resp->url, 301);
        } else {
            abort(404);
        }
    }

}
